<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\CargaService;


class CargaController extends Controller
{
    private $cargaService;
    /** Injeção de dependência para o service */
    public function __construct(CargaService $cargaService) {
        $this->cargaService = $cargaService;
    }

    /** Chamada para os servies responsável pela carga*/
    public function index(){
        $response = $this->cargaService->carga();
        return response()->json($response);
    }
}
