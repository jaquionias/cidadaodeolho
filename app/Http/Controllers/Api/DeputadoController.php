<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\DeputadoService;

class DeputadoController extends Controller
{
    private $deputadoService;
    /** Injeção de dependência para o service */
    public function __construct(DeputadoService $deputadoService) {
        $this->deputadoService = $deputadoService;
    }

    /** Chamada para os servies */
    public function top5(){
        $response = $this->deputadoService->top5();
        return response()->json($response);
    }

    public function ranking(){
        $response = $this->deputadoService->ranking();
        return response()->json($response);
    }
}
