<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

class CargaService
{
    public function carga()
    {
        $contadorRequeste = 0;
        try {
            $response = Http::get("http://dadosabertos.almg.gov.br/ws/deputados/lista_telefonica?formato=json");
        } catch (\Exception $e) {
            return $response['erro'] = $e->getMessage();
        }

        DB::table('redessociais')->delete();
        DB::table('verbas_indenizatorias')->delete();

        /** loop na lista deputados para capturar o id individual */
        foreach ($response->json()['list'] as $data) {
            /** loop para popular a tabela de redes sociais dos deputados */
            foreach ($data['redesSociais'] as $redeSocial) {
                DB::table('redessociais')->insert([
                    'nome' => $redeSocial,
                    'id_deputado' => $data['id'],
                    'nome_deputado' => $data['nome'],
                ]);
            }

            /** Controlador do fluxo de requisições para evitar bloqueio no servidor da API da Assembléia */
            if ($contadorRequeste == 90) {
                sleep(2);
                $contadorRequeste = 0;
            }

            /** loop para popular tabela de reembolso de verbas */
            for ($i = 1; $i <= 12; $i++) {
                $contadorRequeste++;
                $verbas = Http::get("http://dadosabertos.almg.gov.br/ws/prestacao_contas/verbas_indenizatorias/deputados/" . $data['id'] . "/2019/" . $i . "?formato=json");
                if (!$verbas['list'] == []) {
                    $dados = $verbas->json()['list'];
                    foreach ($dados[0]['listaDetalheVerba'] as $verba) {
                        DB::table('verbas_indenizatorias')->insert([
                            'nome_deputado' => $data['nome'],
                            'id_deputado' => $data['id'],
                            'data_referencia' => $verba['dataEmissao']['$'],
                        ]);
                    }
                }
            }
        }
        return true;
    }
}
