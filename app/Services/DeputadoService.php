<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;

class DeputadoService
{
    /** Service para listagem dos 5 deputados que mais solicitaram reembolso por mês */
    public function top5()
    {
        for ($i = 1; $i <= 12; $i++) {
            $data[$i] = DB::table("verbas_indenizatorias")
                ->select(DB::raw("count(id_deputado) as total"), DB::raw("MONTH(data_referencia) as dates"),
                    DB::raw("id_deputado"),
                    DB::raw("nome_deputado"))
                ->whereMonth("data_referencia", '=', $i)
                ->groupBy("dates", 'id_deputado', 'nome_deputado')
                ->orderBy("total", "desc")
                ->limit(5)
                ->get();
        }
        return $data;
    }

    /** Service para classificação das redes sociais mais utilizadas pelos deputados */
    public function ranking()
    {
        $data = DB::table("redessociais")
            ->select(DB::raw("count(id) as total"), DB::raw("nome"))
            ->groupBy("nome")
            ->orderBy("total", "desc")
            ->get();

        return $data;
    }
}
