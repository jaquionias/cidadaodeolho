<?php

Route::group(['namespace' => 'Api'], function () {
    /** Rota para consumir a API da Assembleia */
    Route::get('/carga', 'CargaController@index')->name('index');

    /** Rotas para os webservices */
    Route::get('/top5', 'DeputadoController@top5')->name('top5');
    Route::get('/ranking', 'DeputadoController@ranking')->name('ranking');
});

