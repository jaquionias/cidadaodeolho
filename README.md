## Setup do projeto
##### Na pasta do projeto
```
composer install
```

##### Configurar .env

```
cp .env.example .env
```

##### Gerar chave da aplicação

```
php artisan key:generate
```

## Criar um banco de dados Mysql localmente

SETAR AS CREDENCIAIS DO BANCO DE DADOS NO ARQUIVO .env;

```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=nome_do_BD
DB_USERNAME=usuario_do_BD
DB_PASSWORD=senha_do_BD
```

### Rodar as migrations para criação da estrutura do banco

```
php artisan migrate
```


### Iniciar projeto
```
php artisan serve
```


# Rotas da API
##### Consumir API da Assembléia Legislativa do Estado de Minas Gerais, salvando no banco de dados local.

```
http://localhost:8000/api/carga
```

###### obs 1.: Trocar "localhost:8000" pelo caminho que o servidor foi startado.
###### obs 2.: Esse processo demora em média 13 minutos, visto que, para cada um dos 77 deputados o sistema tem que verificar cada mês se houve ou não reemboldo. Neste caso, serão 77*12 requisições. Além disso, o servidor da Assembléia as vezes bloqueia requisições continuas. Para tentar contornar a situação, a função responsável pela carga tem um sleep de 2 segundos para cada 90 requisições

##### Link para consumir o top5 dos deputados que mais pediram reembolso de verbas indenizatórias por mês, considerando somente o ano de 2019.

```
http://localhost:8000/api/top5
```

##### Link para consumir o ranking de redes sociais mais usadas pelos deputados.

```
http://localhost:8000/api/ranking
```

###### obs 3.: No momento das cargas de teste, nenhum deputado tinha o campo redesSociais[] com alguma informação.

